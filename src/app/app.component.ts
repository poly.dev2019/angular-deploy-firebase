import { Component, OnInit } from '@angular/core';
import { MDCRipple } from '@material/ripple/index';
// const ripple = new MDCRipple(document.querySelector('.foo-button'));

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'angular-firebase-deploy';
  lorems = [
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid corrupti harum recusandae earum saepe veritatis adipisci quae voluptas quas placeat eveniet fuga sapiente distinctio neque nihil, at voluptatibus debitis magni',
  ];

  ngOnInit(): void {
    // const ripple = new MDCRipple(document.querySelector('.foo-button'));
  }
}
